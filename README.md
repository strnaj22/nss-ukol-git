# NSS Example
By Jakub Strnad
A simple hacking tool to exploit CZU students.

## Technologies used
- Javascript
- CSS
- HTML
- React

## Description
This is a simple hacking tool created by Jakub Strnad. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus rhoncus. Mauris tincidunt sem sed arcu. Aliquam ornare wisi eu metus. Aenean id metus id velit ullamcorper pulvinar. Vivamus porttitor turpis ac leo. Nullam faucibus mi quis velit. Suspendisse nisl. Nullam eget nisl. Vivamus porttitor turpis ac leo. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Fusce suscipit libero eget elit. Fusce aliquam vestibulum ipsum. Duis pulvinar. Sed ac dolor sit amet purus malesuada congue. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Etiam egestas wisi a erat. Aliquam ornare wisi eu metus.

## Setup/Installation Requirements
1. Clone this repository to your desktop using `git clone https://github.com/username/repository-name.git`
2. Navigate to the top level of the directory using `cd repository-name`
3. Open `js/index.html` in your browser

## Known Bugs
- Something is broken.
